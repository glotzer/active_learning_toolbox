# Copyright (c) 2018, the Chengyu Dai @ Glotzer Group

from GPyOpt.acquisitions import AcquisitionBase
import numpy as np

class AcquisitionBinaryT(AcquisitionBase):
    """
    T value acquisition function for active classification with binary classes

    :param model: GPyOpt class of model
    :param space: GPyOpt class of domain
    :param optimizer: optimizer of the acquisition. Should be a GPyOpt optimizer
    :param cost_withGradients: function
    :param epsilon: positive value to make the acquisition more explorative.

    .. Note:: allows to compute the Improvement per unit of cost
    """

    analytical_gradient_prediction = True

    def __init__(self, model, space, optimizer=None, cost_withGradients=None, epsilon=0.01):
        self.optimizer = optimizer
        super(AcquisitionBinaryT, self).__init__(model, space, optimizer,
                                                 cost_withGradients=cost_withGradients)
        self.epsilon = epsilon

    @staticmethod
    def fromConfig(model, space, optimizer, cost_withGradients, config):
        return AcquisitionBinaryT(model, space, optimizer, cost_withGradients,
                                  epsilon=config['epsilon'])

    def _compute_acq(self, x):
        """
        Computes the Expected Improvement per unit of cost
        """
        m, s = self.model.predict(x)
        f_acqu = s / (np.abs(m) + self.epsilon)
        return f_acqu

    def _compute_acq_withGradients(self, x):
        """
        Computes the Expected Improvement and its derivative (has a very easy derivative!)
        """
        fmin = self.model.get_fmin()
        m, s, dmdx, dsdx = self.model.predict_withGradients(x)
        f_acqu = s / (np.abs(m) + self.epsilon)
        df_acqu = (dsdx) / (np.abs(m) + self.epsilon) \
                - (s) * np.sign(m) * dmdx / (np.abs(m) + self.epsilon) / (np.abs(m) + self.epsilon)
        return f_acqu, df_acqu
