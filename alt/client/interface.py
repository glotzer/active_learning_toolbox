import grpc
from protobuf_to_dict import protobuf_to_dict, dict_to_protobuf

from alt.proto.python_build import (study_pb2,
                                    experiment_pb2,
                                    metric_pb2,
                                    service_pb2,
                                    service_pb2_grpc,
                                    )
from alt.client.setting import SERVER_URL
from alt.client.status import raise_to_user_if_fail


########## Study ##########


def createStudyFromDict(studySetting_dict):
    studySetting = dict_to_protobuf(study_pb2.StudySetting,
                                    values=studySetting_dict)
    return createStudy(studySetting)


def createStudy(studySetting):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = studySetting
        response = stub.createStudy(request)

    status_resp = response.statusResponse
    raise_to_user_if_fail(status_resp)

    study_id = response.studyID
    return study_id

def infoStudy(study_id):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = study_pb2.StudyID(studyID=study_id)
        response = stub.infoStudy(request)

    status_resp = response.statusResponse
    raise_to_user_if_fail(status_resp)

    return response


def stopStudy(study_id):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = study_pb2.StudyID(studyID=study_id)
        response = stub.stopStudy(request)

    status_resp = response
    raise_to_user_if_fail(status_resp)

    return True

def shouldStopStudy(study_id):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = study_pb2.StudyID(studyID=study_id)
        response = stub.shouldStopStudy(request)

    status_resp = response
    raise_to_user_if_fail(status_resp)

    return reponse.bool


########## Experiment ##########


def createExperiment(study_id):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = study_pb2.StudyID(studyID=study_id)
        response = stub.createExperiment(request)

    status_resp = response.statusResponse
    raise_to_user_if_fail(status_resp)

    # Return the ExperimentSetting object.
    return response.experimentID

def infoExperiment(study_id, experiment_id):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = experiment_pb2.ExperimentID(
            studyID=study_id,
            experimentID=experiment_id)
        response = stub.infoExperiment(request)

    status_resp = response.statusResponse
    raise_to_user_if_fail(status_resp)

    return response

def stopExperiment(study_id, experiment_id):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = experiment_pb2.ExperimentID(
            studyID=study_id,
            experimentID=experiment_id)
        response = stub.stopStudy(request)

    status_resp = response
    raise_to_user_if_fail(status_resp)

    return True


########## Metric ##########


def updateMetric (study_id, exp_id, metric, note="", expAliasID=None):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = metric_pb2.ExpMetric(
            studyID=study_id,
            experimentID=exp_id,
            metric=metric_pb2.Metric(value=metric),
        )
        if note:
            request.note = note
        if expAliasID:
            request.experimentAliasID = expAliasID
        response = stub.updateMetric(request)

    status_resp = response
    raise_to_user_if_fail(status_resp)

    return True

def finishExperimentWithMetric(study_id, exp_id, metric, note="", expAliasID=None):
    with grpc.insecure_channel(SERVER_URL) as channel:
        stub = service_pb2_grpc.ALTStub(channel)
        request = metric_pb2.ExpMetric(
            studyID=study_id,
            experimentID=exp_id,
            metric=metric_pb2.Metric(value=metric),
        )
        if note:
            request.note = note
        if expAliasID:
            request.experimentAliasID = expAliasID
        response = stub.finishExperimentWithMetric(request)

    status_resp = response
    raise_to_user_if_fail(status_resp)

    return True
