from alt.proto.python_build import status_pb2


FAIL = status_pb2.Status.Value('FAIL')


class FailedRPC(Exception):
    def __init__(self, msg):
        self.msg = msg


def raise_to_user(status_resp):
    msg = "Status Code: {}\n" + "Message: {}"
    msg = msg.format(status_resp.status, status_resp.msg)
    raise FailedRPC(msg)

def raise_to_user_if_fail(status_resp):
    if status_resp.status != FAIL:
        return
    raise_to_user(status_resp)
