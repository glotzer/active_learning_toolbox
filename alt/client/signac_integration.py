import datetime
import fcntl        # For file locking.

from alt.client import interface, utils


def _safe_logging(str_to_append, safe_log_fn):
    SAFE_LOGGING_PREFIX = 'Safe Logger {}: '.format(datetime.datetime.utcnow())

    with open(safe_log_fn, "a") as g:
        fcntl.flock(g, fcntl.LOCK_EX)
        g.write(SAFE_LOGGING_PREFIX + str_to_append)
        fcntl.flock(g, fcntl.LOCK_UN)


class ALTClientProjectBase(object):
    """ Base class of all ALT Client Project for easy Signac Integration. """
    def __init__(self, study_id):
        raise NotImplementedError

    def operation_daemon(self):
        raise NotImplementedError

    def operation_main(self, hparamaList):
        raise NotImplementedError


class ExampleALTClientProj(ALTClientProjectBase):
    def __init__(self, study_id, operation_main, safe_log_fn='alt.log'):
        """ operation_main should take a hparamList, evaluate and return a metric value."""

        self.study_id = study_id
        self.operation_main = operation_main
        self.safe_log_fn = safe_log_fn

    def operation_daemon(self):

        msg = "Daemon started for study {}.".format(self.study_id)
        _safe_logging(msg, self.safe_log_fn)
        
        while not interface.shouldStopStudy(self.study_id):

            # Create new experiment.
            exp_id = interface.createExperiment(self.study_id)
            msg = "Experiment {} created.".format(exp_id)
            _safe_logging(msg, self.safe_log_fn)

            import time
            time.sleep(5)

            # Get new experiment's hparams.
            expSetting = interface.infoExperiment(self.study_id, exp_id)
            msg = "Experiment {} queried with setting {}.".format(
                exp_id, expSetting)
            _safe_logging(msg, self.safe_log_fn)
            exp_hparam = utils.parse_hparam(hparamList)

            # Run main operation to get metric.
            metric_val = self.operation_main(exp_hparam)
            msg = "Experiment {} finished on local machine with metric {}.".format(
                exp_id, metric_val)
            _safe_logging(msg, self.safe_log_fn)

            # Finish exp with metric.
            success = interface.finishExperimentWithMetric(
                study_id,
                exp_id,
                metric,
            )
            if success:
                msg = "Experiment {} with metric {} marked as finished on server successfully."
            else:
                msg = "[CRITICAL] Experiment {} with metric {} failed to upload server state. Please upload manually."
            msg = msg.format(exp_id, metric_val)
            _safe_logging(msg, self.safe_log_fn)

        # After study completed.
        _safe_logging("Study stopped.", self.safe_log_fn)
