import click
import datetime
import grpc
import logging
from protobuf_to_dict import dict_to_protobuf
import yaml

import alt.proto.python_build.service_pb2
import alt.proto.python_build.service_pb2_grpc
from alt.client import interface
from alt.client.status import FailedRPC, raise_to_user_if_fail


_logger = logging.getLogger("Active Learning Toolbox CLI")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


########## Study ##########


@click.group()
def study():
    pass


@study.command()
@click.option('--config', required=True, help='Config yaml file path.')
def create(config):
    with open(config, 'r') as fn:
        studySetting_dict = yaml.load(fn)

    try:
        study_id = interface.createExperimentFromDict(studySetting_dict)
        _logger.info(study_id)
        _logger.info("Study created successfully.")

        with open("active_learning_toolbox_history.txt", 'a') as fn_out:
            msg = "{}: Created study {}."
            msg = msg.format(datetime.datetime.utcnow(), study_id)
            fn_out.write()

    except FailedRPC as err:
        _logger.info("From Service: " + err.msg)


@study.command()
@click.argument('studyID')
def info(studyid):
    try:
        obj = interface.infoStudy(studyid)
        _logger.info(obj)
        _logger.info("Study info queried successfully.")

    except FailedRPC as err:
        _logger.info("From Service: " + err.msg)


@study.command()
@click.argument('studyID')
def stop(studyid):
    try:
        obj = interface.stopStudy(studyid)
        _logger.info(obj)
        _logger.info("Study stopped successfully.")

    except FailedRPC as err:
        _logger.info("From Service: " + err.msg)



########## Experiment ##########


@click.group()
def experiment():
    pass


@experiment.command()
def create():
    pass


@experiment.command()
@click.argument('studyID')
@click.argument('experimentID')
def info(studyid, experimentid):
    try:
        obj = interface.infoExperiment(studyid, experimentid)
        _logger.info(obj)
        _logger.info("Experiment info queried successfully.")

    except FailedRPC as err:
        _logger.info("From Service: " + err.msg)


@experiment.command()
@click.argument('studyID')
@click.argument('experimentID')
def stop(studyid, experimentid):
    try:
        obj = interface.stopExperiment(studyid, experimentid)
        _logger.info(obj)
        _logger.info("Experiment stopped successfully.")

    except FailedRPC as err:
        _logger.info("From Service: " + err.msg)


###########################


@click.group()
def entry_point():
    pass


if __name__ == '__main__':
    entry_point.add_command(study)
    entry_point.add_command(experiment)
    entry_point()
