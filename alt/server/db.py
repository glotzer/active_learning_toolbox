import pymongo

PASSWORD = "pineapple"
ATLAS_ADDR = "mongodb://alt_server:{}@glotzerlabmongo-shard-00-00-vjefj.mongodb.net:27017,glotzerlabmongo-shard-00-01-vjefj.mongodb.net:27017,glotzerlabmongo-shard-00-02-vjefj.mongodb.net:27017/test?ssl=true&replicaSet=GlotzerLabMongo-shard-0&authSource=admin&retryWrites=true"
ATLAS_ADDR = ATLAS_ADDR.format(PASSWORD)

def get_collection(client, collection_name):
    collection = getattr(client.alt, collection_name, None)
    if not collection:
        raise AttributeError("{} is not in database.".format(collection_name))
    return collection

def get_exp_collection(client, study_id):
    collection = getattr(client.alt_experiments, study_id, None)
    if collection is None:
        raise AttributeError('No experiment collection is found for study {}.'.format(study_id))
    return collection

def get_all_exp_in_study(client, study_id):
    exp_collection = get_exp_collection(client, study_id)
    return list(exp_collection.find())
