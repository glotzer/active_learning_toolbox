""" Exceptions that can be broadcasted to user clients through StatusResponse object.
"""

from alt.proto.python_build import status_pb2


def raise_to_client(resp_cls, ext_exception_cls):
    if not issubclass(ext_exception_cls, ExtException):
        msg = "{} is not an External Exception that can be exposed to clients."
        msg = msg.format(ext_exception_cls)
        raise ValueError(msg)

    resp = resp_cls()
    if resp_cls is status_pb2.StatusResponse:
        resp.status = ext_exception_cls.status
        resp.msg = ext_exception_cls.msg      
    else:
        resp.statusResponse.status = ext_exception_cls.status
        resp.statusResponse.msg = ext_exception_cls.msg
    return resp


class ExtException(object):

    @classmethod
    def genStatusResponse(cls):
        return status_pb2.StatusResponse(
            status=cls.status,
            msg=cls.msg,
        )

class StudyAlreadyExistError(ExtException):
    msg = "The study already exists."
    status = status_pb2.Status.Value('FAIL')


class StudyNotFoundError(ExtException):
    msg = "The study was not found."
    status = status_pb2.Status.Value('FAIL')


class ExperimentAlreadyExistError(ExtException):
    msg = "The experiment already exists."
    status = status_pb2.Status.Value('FAIL')


class ExperimentNotFoundError(ExtException):
    msg = "The experiment was not found."
    status = status_pb2.Status.Value('FAIL')


class ExperimentAlreadyFinishedError(ExtException):
    msg = "The experiment has already finished. Metrics can't be changed or updated any more."
    status = status_pb2.Status.Value('FAIL')


class HparamNameDuplicateError(ExtException):
    msg = """ The hyperparameters must have distinct names
              to ensure the correct serialization order in server.
          """
    status = status_pb2.Status.Value('FAIL')
