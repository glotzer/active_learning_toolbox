from alt.server.core.external_exception import (raise_to_client, 
                                                HparamNameDuplicateError
                                                )


def hparam_setting_validate(hparamSettingList):
    # Check distinct names.
    name_set = set([hparam.name for hparam in hparamSettingList])
    if len(name_set) != len(hparamSettingList):
        raise_to_client(HparamNameDuplicateError)
