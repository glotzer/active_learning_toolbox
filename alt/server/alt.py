from bson.objectid import ObjectId
import datetime
import munch
from protobuf_to_dict import (protobuf_to_dict,
                              dict_to_protobuf
                              )
from pymongo import MongoClient
import logging

from alt.proto.python_build import (service_pb2,
                                    study_pb2,
                                    experiment_pb2,
                                    status_pb2,
                                    )
from alt.proto.python_build.service_pb2_grpc import ALTServicer
from alt.server.logger import log_request
from alt.server.core.external_exception import *
from alt.server import db
from alt.server import optimization
from alt.server import data_validation
from alt.server.status import SUCCESS, SUCCESS_STATS_RESP



_logger = logging.getLogger("ALT Servicer")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def _fill_sucess_statusresp(response):
    response.statusResponse.status = SUCCESS
    response.statusResponse.msg = ''


def _exist(collection_name, key, val):
    if key == '_id':
        val = ObjectId(val)

    with MongoClient(db.ATLAS_ADDR) as client:
        collection = db.get_collection(client, collection_name)
        if collection.find_one({key: val}):
            ret = True
        else:
            ret = False
    return ret


class ALT(ALTServicer):

    def createStudy(self, request, context):
        collection_name = 'study'
        studySetting = request
        resp_cls = study_pb2.StudyID

        studyName = studySetting.studyName
        data_validation.hparam_setting_validate(studySetting.hparamSetting)

        if _exist(collection_name, 'studyName', studyName):
            return raise_to_client(resp_cls, 
                                   StudyAlreadyExistError)

        study_dict = protobuf_to_dict(studySetting)
        study_dict['timeCreated'] = datetime.datetime.utcnow()
        study_dict['stopped'] = False

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_collection(client, collection_name)
            study_id = collection.insert_one(study_dict).inserted_id

        resp = study_pb2.StudyID(studyID=str(study_id),
                                 statusResponse=SUCCESS_STATS_RESP)
        return resp


    def infoStudy(self, request, context):
        _logger.info('Enter info Study')
        collection_name = 'study'
        study_id = request.studyID
        resp_cls = study_pb2.StudySetting

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_collection(client, collection_name)
            study_dict = collection.find_one({"_id": ObjectId(study_id)})

        if study_dict is None:
            return raise_to_client(resp_cls,
                                   StudyNotFoundError)

        study_dict['studyID'] = str(study_dict.pop('_id'))
        response = dict_to_protobuf(study_pb2.StudySetting,
                                    values=study_dict)
        _fill_sucess_statusresp(response)
        return response


    def infoStudy_todict(self, request, context):
        collection_name = 'study'
        study_id = request.studyID
        resp_cls = study_pb2.StudySetting

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_collection(client, collection_name)
            study_dict = collection.find_one({"_id": ObjectId(study_id)})

        if study_dict is None:
            return raise_to_client(resp_cls,
                                   StudyNotFoundError)

        study_dict['studyID'] = str(study_dict.pop('_id'))
        return study_dict


    def stopStudy(self, request, context):
        collection_name = 'study'
        study_id = request.studyID
        resp_cls = study_pb2.StudyID

        if not _exist(collection_name, '_id', study_id):
            return raise_to_client(resp_cls,
                                   StudyNotFoundError)

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_collection(client, collection_name)
            collection.update_one(
                { "_id": ObjectId(study_id)},
                {
                  '$set': {'timeStopped': datetime.datetime.utcnow(),
                           'stopped': True,
                          }
                }
            )
        return SUCCESS_STATS_RESP


    def createExperiment(self, request, context):
        collection_name = 'experiment'
        study_id = request.studyID
        resp_cls = experiment_pb2.ExperimentSetting

        if _exist(collection_name, '_id', study_id):
            return raise_to_client(resp_cls,
                                   StudyNotFoundError)

        with MongoClient(db.ATLAS_ADDR) as client:
            exps = db.get_all_exp_in_study(client, study_id)
            exps = map(munch.munchify, exps)

            studySetting = self.infoStudy(request, context)

            backend = optimization.GPyOptBackend()
            hparamValue = backend.getNextExp(studySetting, exps)

            print('hparamValue')
            print(hparamValue)
            collection = db.get_exp_collection(client, study_id)
            exp_dict = {
                'studyID': study_id,
                'hparamValue': hparamValue,
                'timeCreated': datetime.datetime.utcnow(),
                'latestMetric': {},
                'fullMetricHistory': [],
                'stopped': False,
            }
            exp_id = collection.insert_one(exp_dict).inserted_id

        resp = experiment_pb2.ExperimentID(studyID=study_id,
                                           experimentID=str(exp_id),
                                           statusResponse=SUCCESS_STATS_RESP)
        return resp

    def infoExperiment(self, request, context):
        collection_name = 'experiment'
        study_id = request.studyID
        exp_id = request.experimentID
        resp_cls = experiment_pb2.ExperimentSetting

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_exp_collection(client, study_id)
            exp_dict = collection.find_one({"_id": ObjectId(exp_id)})

        if exp_dict is None:
            return raise_to_client(resp_cls,
                                   ExperimentNotFoundError)

        exp_dict['experimentID'] = str(exp_dict.pop('_id'))
        response = dict_to_protobuf(experiment_pb2.ExperimentSetting,
                                    values=exp_dict)
        _fill_sucess_statusresp(response)

        return response


    def stopExperiment(self, request, context):
        collection_name = 'experiment'
        study_id = request.studyID
        exp_id = request.experimentID
        resp_cls = experiment_pb2.ExperimentSetting

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_exp_collection(client, study_id)
            collection.update_one(
                { "_id": ObjectId(exp_id)},
                {
                  '$set': {'timeStopped': datetime.datetime.utcnow(),
                           'stopped': True,
                          }
                }
            )
        return SUCCESS_STATS_RESP


    def updateMetric(self, request, context):
        collection_name = 'experiment'
        study_id = request.studyID
        exp_id = request.experimentID
        resp_cls = status_pb2.StatusResponse

        metric = request.metric
        metric.filled = True

        metric = protobuf_to_dict(metric)
        metric['timeCreated'] = datetime.datetime.utcnow()

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_exp_collection(client, study_id)

            exp = collection.find_one({ "_id": ObjectId(exp_id)})
            if exp['stopped']:
                return raise_to_client(resp_cls,
                                       ExperimentAlreadyFinishedError)

            # Update `latestMetric` and `fullMetricHistory` in one trasaction.
            collection.update_one(
                {"_id": ObjectId(exp_id)},
                {'$set': {'latestMetric': metric},
                 '$push': {'fullMetricHistory': metric},
                },
                upsert=False,
            )

        return SUCCESS_STATS_RESP

 
    def finishExperimentWithMetric(self, request, context):
        collection_name = 'experiment'
        study_id = request.studyID
        exp_id = request.experimentID
        resp_cls = status_pb2.StatusResponse

        metric = request.metric
        metric.filled = True

        metric = protobuf_to_dict(metric)
        metric['timeCreated'] = datetime.datetime.utcnow()

        with MongoClient(db.ATLAS_ADDR) as client:
            collection = db.get_exp_collection(client, study_id)

            exp = collection.find_one({ "_id": ObjectId(exp_id)})
            if exp['stopped']:
                return raise_to_client(resp_cls,
                                       ExperimentAlreadyFinishedError)

            # Update `latestMetric` and `fullMetricHistory` in one trasaction.
            collection.update_one(
                {"_id": ObjectId(exp_id)},
                {'$set': {'latestMetric': metric},
                 '$push': {'fullMetricHistory': metric},
                },
                upsert=False,
            )
        return self.stopExperiment(request, context)
