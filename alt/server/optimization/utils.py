from itertools import chain
import numpy as np


def filter_exps(exp_list):
    pending_exps = []       # Running exp but has not return any metric.
    running_exps = []       # Running exp and with partial metric.
    finished_exps = []      # Finished exp with final metric.

    for exp in exp_list:
        if exp.latestMetric and exp.stopped:
            finished_exps.append(exp)
            continue

        if exp.latestMetric and (not exp.stopped):
            running_exps.append(exp)
            continue

        if (not exp.latestMetric) and (not exp.stopped):
            pending_exps.append(exp)
            continue
    
    return pending_exps, running_exps, finished_exps



def pack_hparamValueList(X, hparamSettingList):
    """ [Critical]: The orders of X and hparamSettingList are meaningful and preserved in both server and clients.
        The information is used to pack and unpack hparamValueList objects.
    """
    X = X.ravel()

    if len(X) != len(hparamSettingList):
        raise ValueError('Packing error when packing hparam values, len is not the same in setting and values.')

    hparamValueList = []
    for hparamSetting, value in zip(hparamSettingList, X):
        print(hparamSetting)
        hparamValueList.append({
            'name': hparamSetting.name,
            'value': value,
            'type': hparamSetting.type,
        })
    return hparamValueList


def extract_hparamValue_from_exps(hparamSettingList, pending_exps, running_exps, finished_exps):
    pending_X_list = [extract_hparamValue_single(hparamSettingList, exp) \
                      for exp in chain(pending_exps, running_exps)]

    finished_X_list = [extract_hparamValue_single(hparamSettingList, exp) \
                       for exp in finished_exps]

    if pending_X_list:
        pending_X = np.vstack(pending_X_list)
    else:
        pending_X = None
    
    if finished_X_list:
        finished_X = np.vstack(finished_X_list)
    else:
        finished_X = None

    finished_Y = np.array(
        [extract_latestMetricValue_single(exp) for exp in finished_exps]
    )
    if not finished_Y:
        finished_Y = None

    return pending_X, finished_X, finished_Y

def extract_hparamValue_single(hparamSettingList, exp):
    X_list = []
    valueList = exp.hparamValue
    valueDict = {hparam.name: hparam.value for hparam in valueList}

    for hparamSetting in hparamSettingList:
        X_list.append(valueDict[hparamSetting.name])

    return np.array(X_list)

def extract_latestMetricValue_single(exp):
    return exp.latestMetric.value
