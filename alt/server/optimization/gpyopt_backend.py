import GPyOpt

from alt.server.optimization import backend_base
from alt.server.optimization import utils
from alt.proto.python_build import study_pb2


class GPyOptBackend(backend_base.OptBackendBase):

    NUM_CORES = 4
    INIT_DESIGN_NUMDATA = 10
    EVALUATOR_TYPE = 'local_penalization'

    def shouldStopStudy(self, studySetting, exps):
        """ returns bool """
        return NotImplementedError

    def getNextExp(self, studySetting, exps):
        pending, running, finished = utils.filter_exps(exps)

        # Here pending_X would include both pending and running experiments for now.
        pending_X, finished_X, finished_Y = \
            utils.extract_hparamValue_from_exps(
                studySetting.hparamSetting,
                pending,
                running,
                finished
            )

        if self._in_initial_design_phase(studySetting, finished_X, finished_Y, pending_X):
            return self._getNextExp_initial_design(studySetting, finished_X, finished_Y, pending_X)
        else:
            return self._getNextExp_bayesian_opt(studySetting, finished_X, finished_Y, pending_X)


    def _in_initial_design_phase(self, studySetting, finished_X, finished_Y, pending_X):
        num_scheduled_exps = 0
        num_scheduled_exps += len(finished_Y) if finished_Y is not None else 0
        num_scheduled_exps += pending_X.shape[0] if pending_X is not None else 0
        return num_scheduled_exps <= GPyOptBackend.INIT_DESIGN_NUMDATA

    def _getNextExp_initial_design(self, studySetting, finished_X, finished_Y, pending_X):
        hparamSetting = studySetting.hparamSetting

        space = self._parse_hparam(hparamSetting)
        space = GPyOpt.core.task.space.Design_space(space)

        design = GPyOpt.experiment_design.RandomDesign(space)
        new_X = design.get_samples(1)
        return utils.pack_hparamValueList(new_X, hparamSetting)

    def _getNextExp_bayesian_opt(self, studySetting, finished_X, finished_Y, pending_X):
        hparamSetting = studySetting.hparamSetting

        opt_system = self._setup_opt_sys(studySetting, finished_X, finished_Y)
        new_X = opt_system.suggest_next_locations(pending_X=pending_X, ignored_X=None)
        new_X = new_X[0, :]
        return utils.pack_hparamValueList(new_X, hparamSetting)

    def _setup_opt_sys(self, studySetting, finished_X, finished_Y):
        config = self._parse_StudySetting(studySetting)
        data = dict(X=finished_X, Y=finished_Y,)
        config.update(data)

        opt_sys = GPyOpt.methods.BayesianOptimization(**config)
        return opt_sys

    def _parse_StudySetting(self, studySetting):
        dummy_f = lambda x: 0

        config_gpyopt_style = dict(
            # Study configurable.
            f=dummy_f,
            domain=self._parse_hparam(studySetting.hparamSetting),
            acquisition_type=self._parse_acquisition(studySetting),
            maximize=(studySetting.goal == study_pb2.StudyGoal.Value('MAXIMIZE')),

            # Class configurable.
            num_cores=GPyOptBackend.NUM_CORES,
            evaluator_type=GPyOptBackend.EVALUATOR_TYPE,
            initial_design_numdata=GPyOptBackend.INIT_DESIGN_NUMDATA,

            # Constants.
            normalize_Y=True,
            batch_size=1,
            acquisition_jitter=0,
        )
        return config_gpyopt_style

    def _parse_hparam(self, hparamSettingList):
        bounds = []

        # TODO(daich): Change to allow for other types of variable.
        # TODO(daich): Change to allow for variable warping.
        for hparamSetting in hparamSettingList:
            setting_dict = {
                'name': hparamSetting.name,
                'type': 'continuous',
                'domain': (hparamSetting.valrange.lower,
                           hparamSetting.valrange.upper),
            }
            bounds.append(setting_dict)
        return bounds

    def _parse_acquisition(self, studySetting):
        ACQUISITION_TYPE = {
            study_pb2.StudyGoal.Value('MAXIMIZE') : 'LCB',
            study_pb2.StudyGoal.Value('MINIMIZE') : 'LCB',
            study_pb2.StudyGoal.Value('BINARYT') : 'BinaryT',
        }
        return ACQUISITION_TYPE[studySetting.goal]
