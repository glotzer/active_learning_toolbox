class OptBackendBase(object):
    """ Abstract class for defining interface of the optimization backend.
    """

    def shouldStopStudy(self, studySetting, exps):
        """ returns bool """
        return NotImplementedError

    def getNextExp(self, studySetting, exps):
        return NotImplementedError
