from concurrent import futures
import grpc
import logging
import time

import alt.proto.python_build.service_pb2_grpc as service_pb2_grpc
import alt.server.logger
from alt.server.alt import ALT
from alt.server.grpcext import intercept_server
from alt.server.logging_interceptor import RequestLoggingInterceptor


_logger = logging.getLogger("gRPC Servicer")
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def main():

    with open('./alt-secret/tls.key', 'rb') as f:
        private_key = f.read()
    with open('./alt-secret/tls.crt', 'rb') as f:
        certificate_chain = f.read()
    server_credentials = grpc.ssl_server_credentials(
        ((private_key, certificate_chain),))

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=24))
    service_pb2_grpc.add_ALTServicer_to_server(ALT(), server)

    # logging_interceptor = RequestLoggingInterceptor()
    # server = intercept_server(server, logging_interceptor)
    
    server.add_insecure_port('[::]:5000',)
    # server.add_secure_port('[::]:5000', server_credentials)
    server.start()
    _logger.info('Server successfully started. Listening on port 5000.')
    # server.start() will not block, a sleep-loop is added to keep alive

    while True:
        time.sleep(3600 * 24)
    _logger.info('Out of infinite loop, should not be here.')


if __name__ == "__main__":
    main()
