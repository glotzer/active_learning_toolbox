# Active Learning Toolbox

Maintainer: Chengyu Dai

### CI/CD

The testing and deployment pipeline are hosted on GitLab for free on a mirrored GitLab repo while the source still stays in our BitBucket. However, the deployed service is on Google's Kubernetes Engine with 3 nodes of n1-standard-2 machines. The deployment will be changed by the end of 2018 into a managed billing service.

[![pipeline status](https://gitlab.com/daich/alt/badges/master/pipeline.svg)](https://gitlab.com/daich/alt/commits/master)
[![coverage report](https://gitlab.com/daich/alt/badges/master/coverage.svg)](https://gitlab.com/daich/alt/commits/master)

### Install

Currently the client and server live in the same repo, which is by design the simplest way to manage for an easy to use and understand internal tool. To install,

```bash
$ pip install -r requirements.txt
$ python setup.py install
```

### Usage

A typical workflow includes the following: 
1. Create a study to server and record the `studyID` returned;
2. Create an experiment for that study and record the `experimentID`;
3. Use the ids to query the hyperparams for the experiment and evaluate your experiment locally;
4. Finish the experiment with metric value (from local experiment evaluation);
5. Repeat to 2.

```bash
$ python -m alt.client.cli study create --config xxx.yml
```

For all the other interfaces needed, please refer to `alt.client.interface` module.

### Architecture
    * `gRPC` for API development.
    * `Protocol Buffers 3` for IDL and API contract specification.
    * `Kubernetes on GKE (Google Kubernetes Engine)` for cluster and service management.
