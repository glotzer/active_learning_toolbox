from setuptools import setup, find_packages

setup(
    name = 'alt',
    version = '0.0.1',
    author = 'Chengyu Dai @ Glotzer Lab',
    author_email = 'daich@umich.edu',
    packages = find_packages(),
)