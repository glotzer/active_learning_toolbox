# Taken from official gRPC-python Dockerfile
# https://github.com/grpc/grpc-docker-library/blob/master/1.15.0/python/alpine/Dockerfile

# Standard docker alpine with python 3.6 for GPy compatibility.

## Use prebuilt registry.gitlab.com/daich/alt:base as our new fast build parent image.
# FROM python:3.6 as base
FROM registry.gitlab.com/daich/alt:base as base
RUN pip install protobuf==3.6.1

WORKDIR /usr/src/app
RUN rm -rf /usr/src/app/*

# Onbuild commands:
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# If not exposed might cause kubernetes to keep restarting you.
EXPOSE 5000

ENTRYPOINT [ "python", "-m", "alt.server.service" ]

CMD [ "python", "-m", "alt.server.service" ]
